package com.kdj.karaokenights.retrofit;


import com.kdj.karaokenights.Model.Events_model;
import com.kdj.karaokenights.Model.LoginModel;
import com.kdj.karaokenights.Model.PendingApprovalModel;
import com.kdj.karaokenights.Model.SongRequestModel;
import com.kdj.karaokenights.Model.Songs_model;
import com.kdj.karaokenights.Model.TonightListModel;
import com.kdj.karaokenights.Model.UpdatePendingModel;
import com.kdj.karaokenights.Model.UserCountModel;
import com.kdj.karaokenights.Model.UserRegistrationModel;
import com.kdj.karaokenights.Model.YourRequestModel;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;


/**
 * Created by HP PC on 31-08-2017.
 */

public interface RetrofitInterface {

    @POST("/login.php")
    @FormUrlEncoded
    Observable<ArrayList<LoginModel>> login(@Field("pass") String password, @Field("mob") String mobile);

    @POST("/user_song_count.php")
    @FormUrlEncoded
    Observable<ArrayList<UserCountModel>> getSongCounter(
            @Field("status") String status, @Field("user_id") String userID);

    @POST("/tonights_list.php")
    @FormUrlEncoded
    Observable<ArrayList<TonightListModel>> getTonightsList(
            @Field("status") String status, @Field("event_id") String eventID);

    @POST("/user_request.php")
    @FormUrlEncoded
    Observable<ArrayList<YourRequestModel>> getUserRequest(@Field("user_id") String userID, @Field("event_id") String eventID);

    @GET("/get_event_list.php")
    Observable<ArrayList<Events_model>> getEventList();

    @POST("/events_registration.php")
    @FormUrlEncoded
    Observable<ArrayList<Events_model>> eventRegistration(@Field("id") String id, @Field("event") String event, @Field("kdj") String kdj, @Field("date") String date);

    @POST("/new_user.php")
    @FormUrlEncoded
    Observable<ArrayList<UserRegistrationModel>> userRegistration(@Field("user") String userName, @Field("pass") String password, @Field("email") String email,
                                                                  @Field("mob") String mobileNo, @Field("image") String image);


    @GET()
    Observable<ArrayList<Songs_model>> getSearchResult(@Url String url);

    @POST("/update_user.php")
    @FormUrlEncoded
    Observable<ArrayList<Object>> updateUserDetails(/*@Part MultipartBody.Part image, @Part("email") RequestBody email, @Part("mobile") RequestBody mobile, @Part("id") RequestBody id*/
            @Field("image") String image, @Field("email") String email, @Field("mobile") String mobile, @Field("id") String id);

    @POST("/send_request.php")
    @FormUrlEncoded
    Observable<ArrayList<SongRequestModel>> songRequest(@Field("singer_name") String singlerName, @Field("song_name") String songName, @Field("event_id") String eventID, @Field("date") String date,
                                                        @Field("user_id") String userID, @Field("status") String status, @Field("time") String time, @Field("name") String userNameRequest, @Field("request_name") String requestUserName);

    @POST("/pending_approval.php")
    @FormUrlEncoded
    Observable<ArrayList<PendingApprovalModel>> getPendingApproval(@Field("event_id") String eventID, @Field("status") String status);

    @POST("/update_pending.php")
    @FormUrlEncoded
    Observable<UpdatePendingModel> updatePendingRequest(@Field("id") String id, @Field("status") String status);

    @POST("/delete_event.php")
    @FormUrlEncoded
    Observable<Object> deleteEvent(@Field("id") String id);

    @POST("/update_tonights_list.php")
    @FormUrlEncoded
    Observable<UpdatePendingModel> updateTonightsList(@Field("id") String id, @Field("isPlayed") String isPlayed);
}
