package com.kdj.karaokenights.retrofit;

import com.kdj.karaokenights.ASPModel.EventModel;
import com.kdj.karaokenights.ASPModel.SongModel;
import com.kdj.karaokenights.ASPModel.SongSearchModel;
import com.kdj.karaokenights.ASPModel.SuccessModel;
import com.kdj.karaokenights.activity.model.UserModel;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

/**
 * Created by Vaibhav Barad on 7/6/2018.
 */
public interface AspNetRetrofitClient {

    @POST("/Karaoke_API/api/User/login")
    Single<UserModel> login(@Body UserModel userData);

    @POST("/Karaoke_API/api/User/register")
    Observable<UserModel> userRegistration(@Body UserModel userData);

    @GET("/Karaoke_API/api/Admin/Event")
    Observable<ArrayList<EventModel>> getEventList();

    @POST("/Karaoke_API/api/Admin/Event")
    Observable<SuccessModel> eventRegistration(@Body EventModel eventData);

    @POST("/Karaoke_API/api/Admin/Event/delete")
    Observable<Object> deleteEvent(@Body EventModel eventData);

    @GET("/Karaoke_API/api/Admin/Event/{userID}")
    Observable<ArrayList<SongModel>> getSongCounter(@Path("userID") int userID);

    @GET("/Karaoke_API/api/Songs/{evtID}")
    Observable<ArrayList<SongModel>> getTonightsList(@Path("evtID") String evtID);

    @POST("/Karaoke_API/api/Songs/play")
    Observable<SuccessModel> updateTonightsList(@Body SongModel songData);

    @POST("/KaraokeApiImg/api/Values")
    Observable<SuccessModel> updateUserDetails(@Body UserModel userData);

    @GET("/Karaoke_API/api/Admin/pending/{eventID}")
    Observable<ArrayList<SongModel>> getPendingApproval(@Path("eventID") int eventID);

    @POST("/Karaoke_API/api/Admin/status")
    Observable<SuccessModel> updatePendingRequest(@Body SongModel songData);

    @GET("/Karaoke_API/api/user/songs/{userID}/{eventID}")
    Observable<ArrayList<SongModel>> getUserRequest(@Path("userID") String userID, @Path("eventID") String eventID);

    @GET("/Karaoke_API/api/User/find/{username}")
    Observable<ArrayList<UserModel>> searchUser(@Path("username") String userName);

    @GET()
    Observable<SongSearchModel> searchSong(@Url String url);

    @POST("/Karaoke_API/api/Songs")
    Observable<SuccessModel> songRequest(@Body SongModel model);

}
