package com.kdj.karaokenights.retrofit;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by HP PC on 31-08-2017.
 */

public class RetrofitApiUtils {

    public static RequestBody JSONToRetrofitJSON(String JSONRequest) throws JSONException {
        return RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (new JSONObject(JSONRequest)).toString());
    }

   /* public static RetrofitInterface getGeneralAPIService() {
        return RetrofitClient.getClient("http://kdj.kwrk.in/").create(RetrofitInterface.class);
    }

    public static AspNetRetrofitClient getASPNETGeneralAPIService() {
        return RetrofitClient.getClient("http://test.ninestack.com/").create(AspNetRetrofitClient.class);
    }*/

    /**
     *
     * @param unPreparedObservable pass the {@link RetrofitInterface} function that you want to call and cast the observable to that function
     * @return an observable to attach too, to get the result
     */
    public static Observable<?> getPreparedObservable(Observable<?> unPreparedObservable) {
        Observable<?> preparedObservable = unPreparedObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
            preparedObservable = preparedObservable.cache();
        return preparedObservable;
    }
}
