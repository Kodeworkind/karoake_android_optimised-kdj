package com.kdj.karaokenights.repository;

import com.kdj.karaokenights.activity.model.UserModel;
import com.kdj.karaokenights.retrofit.AspNetRetrofitClient;
import com.kdj.karaokenights.retrofit.RetrofitInterface;

import io.reactivex.Single;
import retrofit2.Retrofit;

/**
 * Created by Adrian Almeida.
 */
public class RemoteRepo implements DataSource{

    public static final String TAG = RemoteRepo.class.getSimpleName();

    Retrofit mRetrofit;
    private RetrofitInterface retrofitAPI;
    private AspNetRetrofitClient aspNetRetrofitClient;

    public RemoteRepo(Retrofit retrofit) {
        this.mRetrofit = retrofit;
        this.retrofitAPI = retrofit.create(RetrofitInterface.class);
        aspNetRetrofitClient=retrofit.create(AspNetRetrofitClient.class);
    }


    @Override
    public Single<UserModel> login(UserModel userModel) {
        return aspNetRetrofitClient.login(userModel);
    }
}
