package com.kdj.karaokenights.repository;

import android.content.Context;

import com.kdj.karaokenights.activity.model.UserModel;
import com.kdj.karaokenights.retrofit.RetrofitClient;

import io.reactivex.Single;


/**
 * Created by Adrian Almeida.
 */
public class Repository implements DataSource {

    private final DataSource mRemoteDataSource;
    private final DataSource mLocalDataSource;

    public Repository(Context applicationContext,int url) {
        this.mRemoteDataSource = new RemoteRepo(RetrofitClient.getClient(url));
        this.mLocalDataSource = new LocalRepo(applicationContext);
    }

    @Override
    public Single<UserModel> login(UserModel userModel) {
        return mRemoteDataSource.login(userModel);
    }
}
