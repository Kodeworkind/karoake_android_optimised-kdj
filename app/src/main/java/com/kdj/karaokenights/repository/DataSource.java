package com.kdj.karaokenights.repository;

import com.kdj.karaokenights.activity.model.UserModel;

import io.reactivex.Single;

/**
 * Created by Adrian Almeida.
 */
public interface DataSource {
    Single<UserModel> login(UserModel userModel);


}
