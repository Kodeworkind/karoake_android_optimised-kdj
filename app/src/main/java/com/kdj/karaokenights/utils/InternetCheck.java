package com.kdj.karaokenights.utils;

import android.content.Context;
import android.net.NetworkInfo;

import com.github.pwittchen.reactivenetwork.library.rx2.Connectivity;
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by Adrian Almeida.
 */
public class InternetCheck {
    public static Disposable networkDisposable;
    private static InternetCheck.netCheck netCheck;


    public InternetCheck(InternetCheck.netCheck netCheck) {
        this.netCheck = netCheck;
        this.networkDisposable = networkDisposable;
    }

    public interface netCheck {
        void internetAvailable();

        void noInternet();
    }

    public static void observeInternet(Context context) {
        networkDisposable = ReactiveNetwork.observeNetworkConnectivity(context)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new io.reactivex.functions.Consumer<Connectivity>() {
                    @Override
                    public void accept(Connectivity connectivity) throws Exception {
                        final NetworkInfo.State state = connectivity.getState();
                        final String name = connectivity.getTypeName();
                        if (!connectivity.isAvailable()) {
                            netCheck.noInternet();
                        } else {
                            netCheck.internetAvailable();
                        }
                    }
                });

    }


    public static void disposeDisposable() {
        safelyDispose(networkDisposable);
    }

    private static void safelyDispose(Disposable... disposables) {
        for (Disposable subscription : disposables) {
            if (subscription != null && !subscription.isDisposed()) {
                subscription.dispose();
            }
        }
    }
}
