package com.kdj.karaokenights.utils;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.kdj.karaokenights.R;


/**
 * Created by HP PC on 05-01-2018.
 */

public class CustomProgressDialog {
    public static AlertDialog dialog;
    public static AlertDialog.Builder dialogBuilder;
    static TextView message;

    /**
     * ShowProgressDialog
     * @param context
     * @param progressMessage
     */
    public static void ShowProgressDialog(Context context, String progressMessage) {
        dialogBuilder = new AlertDialog.Builder(context, R.style.CustomDialog);

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.progress, null);
        message = (TextView) dialogView.findViewById(R.id.message);
        if (progressMessage != null) {
            message.setText(progressMessage);
        }
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        dialog = dialogBuilder.create();
        dialog.show();
    }

    /**
     *  Hide progress dialog
     */
    public static void HideProgressDialog() {
        if(dialog!=null && dialog.isShowing()){
            dialog.dismiss();
        }
    }
}
