package com.kdj.karaokenights.activity.login;

import com.kdj.karaokenights.activity.model.UserModel;
import com.kdj.karaokenights.mvp.BasePresenter;

/**
 * Created by Adrian Almeida.
 */
public class LoginContract {

    interface View {
        void onLoginSuccess(UserModel userModel);

        void onLoginFailure();

        void showError(String message, int inputField);

    }

    public interface LeaderBoardPresenter extends BasePresenter<View> {
        void login(String email,String password);
    }
}
