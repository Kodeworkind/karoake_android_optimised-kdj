package com.kdj.karaokenights.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.kdj.karaokenights.R;
import com.kdj.karaokenights.activity.events.Events;
import com.kdj.karaokenights.activity.login.LoginActivity;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SplashScreen extends AppCompatActivity {

    @BindView(R.id.textView)
    TextView powered;
    @BindView(R.id.textView2)
    TextView powered2;

    private static int SPLASH_TIME_OUT = 5000;
    String mob_pref;
    boolean isLoggedIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);


        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Lato-LightItalic.ttf");
        Typeface custom_font2 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Bold.ttf");
        SharedPreferences prefs = getSharedPreferences("Login-Info", MODE_PRIVATE);

        isLoggedIn = prefs.getBoolean("loginDone", false);

        powered.setTypeface(custom_font);
        powered2.setTypeface(custom_font2);


        new Handler().postDelayed(new Runnable() {
            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = null;
                if (isLoggedIn) {
                    i = new Intent(SplashScreen.this, Events.class);
                } else {
                    i = new Intent(SplashScreen.this, LoginActivity.class);
                }
                startActivity(i);
                finish();
            }
        }, SPLASH_TIME_OUT);

    }
}
