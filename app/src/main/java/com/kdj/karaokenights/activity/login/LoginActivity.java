package com.kdj.karaokenights.activity.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kdj.karaokenights.R;
import com.kdj.karaokenights.activity.events.Events;
import com.kdj.karaokenights.activity.model.UserModel;
import com.kdj.karaokenights.activity.register.RegisterActivity;
import com.kdj.karaokenights.repository.Repository;
import com.kdj.karaokenights.utils.CustomProgressDialog;
import com.kdj.karaokenights.utils.InternetCheck;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements InternetCheck.netCheck, LoginContract.View {

    @BindView(R.id.editText)
    EditText et_email;
    @BindView(R.id.editText3)
    EditText et_password;
    @BindView(R.id.login)
    Button btn_login;
    @BindView(R.id.tv_register_label)
    TextView tv_register_label;
    @BindView(R.id.main_content)
    CoordinatorLayout main_content;


    public static String password, email;
    Snackbar snackbar;
    private InternetCheck internetCheck;
    private boolean internetAvailability;
    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        loginPresenter = new LoginPresenter(this, new Repository(this.getApplicationContext(), 1));
        internetCheck = new InternetCheck(LoginActivity.this);

        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Medium.ttf");
        Typeface custom_font2 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Hairline.ttf");
        Typeface custom_font3 = Typeface.createFromAsset(getAssets(), "fonts/Lato-LightItalic.ttf");


        et_email.setTypeface(custom_font2);
        et_password.setTypeface(custom_font2);
        tv_register_label.setTypeface(custom_font3);
        btn_login.setTypeface(custom_font);

        SharedPreferences prefs = getSharedPreferences("Login-Info", MODE_PRIVATE);

        String mob_pref = prefs.getString("mob", "");
        String pass_pref = prefs.getString("pass", "");
        boolean isLoggedIn = prefs.getBoolean("loginDone", false);


        if (mob_pref != "" && isLoggedIn) {
            password = pass_pref;
            email = mob_pref;
            startActivity(new Intent(LoginActivity.this, Events.class));
            finish();

        }

    }

    @OnClick(R.id.register)
    public void OnRegisterClick(View view) {
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
        finish();
    }

    @OnClick(R.id.login)
    public void OnLoginClick(View view) {


        password = et_password.getText().toString();
        email = et_email.getText().toString().trim();

        if (internetAvailability){
            CustomProgressDialog.ShowProgressDialog(LoginActivity.this,null);
            loginPresenter.login(email, password);
        }
        else
            Toast.makeText(LoginActivity.this, "Please check your internet connection", Toast.LENGTH_LONG).show();

    }


    private boolean formFieldValid() {

        if (et_email.getText().toString().trim().isEmpty()) {
            et_email.setError("This is a required field");
            return false;
        }

        if (et_password.getText().toString().isEmpty()) {
            et_password.setError("This is a required field");
            return false;
        }
        return true;
    }


    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (!doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Press again to exit.", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        InternetCheck.observeInternet(LoginActivity.this);
        loginPresenter.takeView(this);
    }

    public void showInternetError() {
        snackbar = Snackbar
                .make(main_content, "Please check your internet connection", Snackbar.LENGTH_INDEFINITE);
        snackbar.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        InternetCheck.disposeDisposable();
    }

    @Override
    protected void onStop() {
        super.onStop();
        loginPresenter.dropView();
    }

    @Override
    public void internetAvailable() {
        internetAvailability = true;
        if (snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }
    }

    @Override
    public void noInternet() {
        showInternetError();
        internetAvailability = false;
    }

    @Override
    public void onLoginSuccess(UserModel loginModel) {
        setPreferences(loginModel);
        CustomProgressDialog.HideProgressDialog();
        startActivity(new Intent(LoginActivity.this, Events.class));
        finish();
    }

    private void setPreferences(UserModel loginModel) {
        SharedPreferences.Editor editor = getSharedPreferences("Login-Info", MODE_PRIVATE).edit();
        editor.putString("admin", loginModel.getType());
        editor.putString("name", loginModel.getName());
        editor.putString("email", loginModel.getEmail());
        editor.putString("mob", loginModel.getPhoneNumber());
        editor.putString("id", String.valueOf(loginModel.getId()));
        editor.putBoolean("loginDone", true);
        editor.commit();
    }

    @Override
    public void onLoginFailure() {
        CustomProgressDialog.HideProgressDialog();
        Toast.makeText(LoginActivity.this, "Invalid Email or Password", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showError(String message, int inputField) {
        CustomProgressDialog.HideProgressDialog();
        switch (inputField) {
            case 1:
                et_email.setError(message);
                break;
            case 2:
                et_password.setError(message);
                break;
            default:
                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
        }
    }
}
