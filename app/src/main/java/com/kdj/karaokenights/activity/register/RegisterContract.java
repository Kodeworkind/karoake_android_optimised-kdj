package com.kdj.karaokenights.activity.register;

import com.kdj.karaokenights.activity.login.LoginContract;
import com.kdj.karaokenights.activity.model.UserModel;
import com.kdj.karaokenights.mvp.BasePresenter;

/**
 * Created by Adrian Almeida.
 */
public class RegisterContract {
    interface View {
        void onRegisterSuccess(UserModel userModel);

        void onRegisterFailure();

        void showError(String message, int inputField);

    }

    public interface RegisterPresenter extends BasePresenter<View> {
        void registerUser(String userName,String email,String mobileNumber,String password);
    }
}
