package com.kdj.karaokenights.activity.register;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kdj.karaokenights.R;

import butterknife.ButterKnife;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
    }
}
