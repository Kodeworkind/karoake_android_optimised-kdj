package com.kdj.karaokenights.activity.login;

import android.util.Base64;
import android.util.Log;
import android.util.Patterns;

import com.kdj.karaokenights.activity.model.UserModel;
import com.kdj.karaokenights.repository.DataSource;
import com.kdj.karaokenights.repository.Repository;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Adrian Almeida.
 */
public class LoginPresenter implements LoginContract.LeaderBoardPresenter {

    CompositeDisposable compositeDisposable;
    private LoginContract.View mView;
    private Disposable loginDisposable;
    private DataSource mDataSource;

    public LoginPresenter(LoginContract.View view, Repository repository) {
        this.mView = view;
        this.mDataSource = repository;
        this.compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void login(String email, String password) {
        if (email.equals("") || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mView.showError("Please enter a valid email", 1);
            return;

        }
        if (password.equals("")) {
            mView.showError("Please enter password", 2);
            return;
        }


        UserModel userModel = new UserModel();
        userModel.setPassword(Base64.encodeToString(password.getBytes(), Base64.DEFAULT));
        userModel.setEmail(email);

        DisposableSingleObserver<UserModel> singleObserver = new DisposableSingleObserver<UserModel>() {
            @Override
            public void onSuccess(UserModel userModel) {
                if (mView != null && userModel != null && userModel.getResult().equalsIgnoreCase("success")) {
                    mView.onLoginSuccess(userModel);
                } else if (mView != null) {
                    mView.onLoginFailure();
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e("daily Match", "error=>>" + e.getMessage());
                if (mView != null)
                    mView.onLoginFailure();
            }
        };

        if (!compositeDisposable.isDisposed()) {
            Single<UserModel> mUserSingle = mDataSource.login(userModel);
            loginDisposable = mUserSingle.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(singleObserver);
            compositeDisposable.add(singleObserver);
        }

    }

    @Override
    public void takeView(LoginContract.View view) {
        this.mView = view;
    }

    @Override
    public void dropView() {
        this.mView = null;
        if (compositeDisposable != null && !compositeDisposable.isDisposed())
            compositeDisposable.clear();
    }
}
