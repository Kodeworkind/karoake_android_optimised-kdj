package com.kdj.karaokenights.ASPModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Vaibhav Barad on 7/9/2018.
 */
public class SongSearchModel implements Parcelable {
    public static final Creator<SongSearchModel> CREATOR = new Creator<SongSearchModel>() {
        @Override
        public SongSearchModel createFromParcel(Parcel in) {
            return new SongSearchModel(in);
        }

        @Override
        public SongSearchModel[] newArray(int size) {
            return new SongSearchModel[size];
        }
    };
    @SerializedName("results")
    @Expose
    public Results results;

    public Results getResults() {
        return results;
    }

    public void setResults(Results results) {
        this.results = results;
    }

    public SongSearchModel(Parcel in) {
        results = in.readParcelable(Results.class.getClassLoader());
    }

    public SongSearchModel() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(results, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static class Results implements Parcelable {
        public static final Creator<Results> CREATOR = new Creator<Results>() {
            @Override
            public Results createFromParcel(Parcel in) {
                return new Results(in);
            }

            @Override
            public Results[] newArray(int size) {
                return new Results[size];
            }
        };
        @SerializedName("trackmatches")
        @Expose
        public Trackmatches trackmatches;

        protected Results(Parcel in) {
            trackmatches = in.readParcelable(Trackmatches.class.getClassLoader());
        }

        public Trackmatches getTrackmatches() {
            return trackmatches;
        }

        public void setTrackmatches(Trackmatches trackmatches) {
            this.trackmatches = trackmatches;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(trackmatches, flags);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static class Trackmatches implements Parcelable {

            public static final Creator<Trackmatches> CREATOR = new Creator<Trackmatches>() {
                @Override
                public Trackmatches createFromParcel(Parcel in) {
                    return new Trackmatches(in);
                }

                @Override
                public Trackmatches[] newArray(int size) {
                    return new Trackmatches[size];
                }
            };
            @SerializedName("track")
            @Expose
            public ArrayList<Track> track = null;

            protected Trackmatches(Parcel in) {
                track = in.createTypedArrayList(Track.CREATOR);
            }

            public ArrayList<Track> getTrack() {
                return track;
            }

            public void setTrack(ArrayList<Track> track) {
                this.track = track;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeTypedList(track);
            }

            @Override
            public int describeContents() {
                return 0;
            }

            public static class Track implements Parcelable {

                public static final Creator<Track> CREATOR = new Creator<Track>() {
                    @Override
                    public Track createFromParcel(Parcel in) {
                        return new Track(in);
                    }

                    @Override
                    public Track[] newArray(int size) {
                        return new Track[size];
                    }
                };
                @SerializedName("name")
                @Expose
                public String name;
                @SerializedName("artist")
                @Expose
                public String artist;
                @SerializedName("url")
                @Expose
                public String url;

                protected Track(Parcel in) {
                    name = in.readString();
                    artist = in.readString();
                    url = in.readString();
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getArtist() {
                    return artist;
                }

                public void setArtist(String artist) {
                    this.artist = artist;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeString(name);
                    dest.writeString(artist);
                    dest.writeString(url);
                }

                @Override
                public int describeContents() {
                    return 0;
                }
            }
        }
    }
}
