package com.kdj.karaokenights.ASPModel;

/**
 * Created by Vaibhav Barad on 7/7/2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class SuccessModel {
    @SerializedName("result")
    @Expose
    public String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}