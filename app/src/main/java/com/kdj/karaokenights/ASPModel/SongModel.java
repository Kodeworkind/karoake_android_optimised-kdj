package com.kdj.karaokenights.ASPModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Vaibhav Barad on 7/7/2018.
 */
public class SongModel implements Parcelable {
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("singer")
    @Expose
    public String singer;
    @SerializedName("eventId")
    @Expose
    public int eventId;
    @SerializedName("status")
    @Expose
    public int status;
    @SerializedName("userId")
    @Expose
    public int userId;
    @SerializedName("isPlayed")
    @Expose
    public int isPlayed;
    @SerializedName("requestName")
    @Expose
    public String requestName;
    @SerializedName("userType")
    @Expose
    public int userType;
    @SerializedName("createdAt")
    @Expose
    public String createdAt;

    public SongModel(Parcel in) {
        id = in.readInt();
        name = in.readString();
        singer = in.readString();
        eventId = in.readInt();
        status = in.readInt();
        userId = in.readInt();
        isPlayed = in.readInt();
        requestName = in.readString();
        userType = in.readInt();
        createdAt = in.readString();
    }

    public SongModel() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(singer);
        dest.writeInt(eventId);
        dest.writeInt(status);
        dest.writeInt(userId);
        dest.writeInt(isPlayed);
        dest.writeString(requestName);
        dest.writeInt(userType);
        dest.writeString(createdAt);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SongModel> CREATOR = new Creator<SongModel>() {
        @Override
        public SongModel createFromParcel(Parcel in) {
            return new SongModel(in);
        }

        @Override
        public SongModel[] newArray(int size) {
            return new SongModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getIsPlayed() {
        return isPlayed;
    }

    public void setIsPlayed(int isPlayed) {
        this.isPlayed = isPlayed;
    }

    public String getRequestName() {
        return requestName;
    }

    public void setRequestName(String requestName) {
        this.requestName = requestName;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
