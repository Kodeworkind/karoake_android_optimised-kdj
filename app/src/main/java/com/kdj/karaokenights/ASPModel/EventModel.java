package com.kdj.karaokenights.ASPModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Vaibhav Barad on 7/7/2018.
 */
public class EventModel implements Parcelable {

    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("eventDate")
    @Expose
    public String eventDate;
    @SerializedName("endDate")
    @Expose
    public String endDate;
    @SerializedName("dj")
    @Expose
    public String dj;
    @SerializedName("isDeleted")
    @Expose
    public int isDeleted;
    @SerializedName("eventImage")
    @Expose
    public String eventImage;

    public EventModel() {
    }

    protected EventModel(Parcel in) {
        id = in.readInt();
        name = in.readString();
        eventDate = in.readString();
        endDate = in.readString();
        dj = in.readString();
        isDeleted = in.readInt();
        eventImage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(eventDate);
        dest.writeString(endDate);
        dest.writeString(dj);
        dest.writeInt(isDeleted);
        dest.writeString(eventImage);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<EventModel> CREATOR = new Creator<EventModel>() {
        @Override
        public EventModel createFromParcel(Parcel in) {
            return new EventModel(in);
        }

        @Override
        public EventModel[] newArray(int size) {
            return new EventModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endTime) {
        this.endDate = endTime;
    }

    public String getDj() {
        return dj;
    }

    public void setDj(String dj) {
        this.dj = dj;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getEventImage() {
        return eventImage;
    }

    public void setEventImage(String eventImage) {
        this.eventImage = eventImage;
    }
}
