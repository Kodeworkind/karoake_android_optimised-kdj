package com.kdj.karaokenights.Model;

/**
 * Created by Vaibhav Barad on 12/5/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdatePendingModel {
    @SerializedName("mesage")
    @Expose
    private String mesage;

    public String getMesage() {
        return mesage;
    }

    public void setMesage(String mesage) {
        this.mesage = mesage;
    }

}