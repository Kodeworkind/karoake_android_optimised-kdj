package com.kdj.karaokenights.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asd on 11/11/2017.
 */

public class UserCountModel implements Parcelable {

    private String name,user_id,status,date,id,event_id,singer_name;
    @SerializedName("song_name")
    @Expose
    private String songName;

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public UserCountModel() {
    }

    protected UserCountModel(Parcel in) {
        songName = in.readString();
        name = in.readString();
        user_id = in.readString();
        status = in.readString();
        date = in.readString();
        id = in.readString();
        event_id = in.readString();
        singer_name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(songName);
        dest.writeString(name);
        dest.writeString(user_id);
        dest.writeString(status);
        dest.writeString(date);
        dest.writeString(id);
        dest.writeString(event_id);
        dest.writeString(singer_name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserCountModel> CREATOR = new Creator<UserCountModel>() {
        @Override
        public UserCountModel createFromParcel(Parcel in) {
            return new UserCountModel(in);
        }

        @Override
        public UserCountModel[] newArray(int size) {
            return new UserCountModel[size];
        }
    };

}
