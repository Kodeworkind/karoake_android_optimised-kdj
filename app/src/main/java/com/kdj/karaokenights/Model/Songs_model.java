

package com.kdj.karaokenights.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Songs_model {


    @SerializedName("status")
    @Expose
    private Status status;
    @SerializedName("pagination")
    @Expose
    private Pagination pagination;
    @SerializedName("data")
    @Expose
    private ArrayList<Datum> data = null;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public ArrayList<Datum> getData() {
        return data;
    }

    public void setData(ArrayList<Datum> data) {
        this.data = data;
    }

    public static class Pagination {

        @SerializedName("count")
        @Expose
        private Long count;
        @SerializedName("total")
        @Expose
        private Long total;
        @SerializedName("offset")
        @Expose
        private Long offset;

        public Long getCount() {
            return count;
        }

        public void setCount(Long count) {
            this.count = count;
        }

        public Long getTotal() {
            return total;
        }

        public void setTotal(Long total) {
            this.total = total;
        }

        public Long getOffset() {
            return offset;
        }

        public void setOffset(Long offset) {
            this.offset = offset;
        }

    }

    public static class Producer {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public static class Status {

        @SerializedName("code")
        @Expose
        private Long code;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("api")
        @Expose
        private String api;

        public Long getCode() {
            return code;
        }

        public void setCode(Long code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getApi() {
            return api;
        }

        public void setApi(String api) {
            this.api = api;
        }

    }

    public static class Writer {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("id")
        @Expose
        private String id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

    }

    public static class Datum {

        @SerializedName("popularity")
        @Expose
        private Float popularity;
        @SerializedName("track_artist_id")
        @Expose
        private String trackArtistId;
        @SerializedName("duration")
        @Expose
        private Long duration;
        @SerializedName("main_genre")
        @Expose
        private String mainGenre;
        @SerializedName("track_spotify_id")
        @Expose
        private String trackSpotifyId;
        @SerializedName("entity_type")
        @Expose
        private String entityType;
        @SerializedName("track_ref_id")
        @Expose
        private String trackRefId;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("original_release_year")
        @Expose
        private Long originalReleaseYear;
        @SerializedName("track_youtube_id")
        @Expose
        private String trackYoutubeId;
        @SerializedName("artist_name")
        @Expose
        private String artistName;
        @SerializedName("isrc")
        @Expose
        private String isrc;
        @SerializedName("track_artist_ref_id")
        @Expose
        private String trackArtistRefId;
        @SerializedName("release_year")
        @Expose
        private Long releaseYear;
        @SerializedName("track_album_id")
        @Expose
        private String trackAlbumId;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("track_album_ref_id")
        @Expose
        private String trackAlbumRefId;
        @SerializedName("album_title")
        @Expose
        private String albumTitle;
        @SerializedName("track_index")
        @Expose
        private String trackIndex;
        @SerializedName("track_musicbrainz_id")
        @Expose
        private String trackMusicbrainzId;
        @SerializedName("writer")
        @Expose
        private ArrayList<Writer> writer = null;
        @SerializedName("producer")
        @Expose
        private ArrayList<Producer> producer = null;

        public Float getPopularity() {
            return popularity;
        }

        public void setPopularity(Float popularity) {
            this.popularity = popularity;
        }

        public String getTrackArtistId() {
            return trackArtistId;
        }

        public void setTrackArtistId(String trackArtistId) {
            this.trackArtistId = trackArtistId;
        }

        public Long getDuration() {
            return duration;
        }

        public void setDuration(Long duration) {
            this.duration = duration;
        }

        public String getMainGenre() {
            return mainGenre;
        }

        public void setMainGenre(String mainGenre) {
            this.mainGenre = mainGenre;
        }

        public String getTrackSpotifyId() {
            return trackSpotifyId;
        }

        public void setTrackSpotifyId(String trackSpotifyId) {
            this.trackSpotifyId = trackSpotifyId;
        }

        public String getEntityType() {
            return entityType;
        }

        public void setEntityType(String entityType) {
            this.entityType = entityType;
        }

        public String getTrackRefId() {
            return trackRefId;
        }

        public void setTrackRefId(String trackRefId) {
            this.trackRefId = trackRefId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Long getOriginalReleaseYear() {
            return originalReleaseYear;
        }

        public void setOriginalReleaseYear(Long originalReleaseYear) {
            this.originalReleaseYear = originalReleaseYear;
        }

        public String getTrackYoutubeId() {
            return trackYoutubeId;
        }

        public void setTrackYoutubeId(String trackYoutubeId) {
            this.trackYoutubeId = trackYoutubeId;
        }

        public String getArtistName() {
            return artistName;
        }

        public void setArtistName(String artistName) {
            this.artistName = artistName;
        }

        public String getIsrc() {
            return isrc;
        }

        public void setIsrc(String isrc) {
            this.isrc = isrc;
        }

        public String getTrackArtistRefId() {
            return trackArtistRefId;
        }

        public void setTrackArtistRefId(String trackArtistRefId) {
            this.trackArtistRefId = trackArtistRefId;
        }

        public Long getReleaseYear() {
            return releaseYear;
        }

        public void setReleaseYear(Long releaseYear) {
            this.releaseYear = releaseYear;
        }

        public String getTrackAlbumId() {
            return trackAlbumId;
        }

        public void setTrackAlbumId(String trackAlbumId) {
            this.trackAlbumId = trackAlbumId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getTrackAlbumRefId() {
            return trackAlbumRefId;
        }

        public void setTrackAlbumRefId(String trackAlbumRefId) {
            this.trackAlbumRefId = trackAlbumRefId;
        }

        public String getAlbumTitle() {
            return albumTitle;
        }

        public void setAlbumTitle(String albumTitle) {
            this.albumTitle = albumTitle;
        }

        public String getTrackIndex() {
            return trackIndex;
        }

        public void setTrackIndex(String trackIndex) {
            this.trackIndex = trackIndex;
        }

        public String getTrackMusicbrainzId() {
            return trackMusicbrainzId;
        }

        public void setTrackMusicbrainzId(String trackMusicbrainzId) {
            this.trackMusicbrainzId = trackMusicbrainzId;
        }

        public ArrayList<Writer> getWriter() {
            return writer;
        }

        public void setWriter(ArrayList<Writer> writer) {
            this.writer = writer;
        }

        public ArrayList<Producer> getProducer() {
            return producer;
        }

        public void setProducer(ArrayList<Producer> producer) {
            this.producer = producer;
        }

    }

}