

package com.kdj.karaokenights.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Events_model {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("kdj")
    @Expose
    private String kdj;
    @SerializedName("event")
    @Expose
    private String event;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("result")
    @Expose
    private Long result;

    public Long getResult() {
        return result;
    }

    public void setResult(Long result) {
        this.result = result;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKdj() {
        return kdj;
    }

    public void setKdj(String kdj) {
        this.kdj = kdj;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}